const {
    src,
    dest,
    series,
    watch,
    build
} = require('gulp');

const config = require('./gulp.config')();
const gulp = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp');

const sass = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-sass');
const autoprefixer = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-autoprefixer');
const sourcemaps = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-sourcemaps');

const handlebars = require('C:/Users/user/AppData/Roaming/npm/node_modules/handlebars');
const gulpHandlebars = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-handlebars-html')(handlebars);
const regexRename = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-regex-rename');
const replace = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-replace');

const browserSync = require('browser-sync').create();

const svgSprite = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-svg-sprite');

const imagemin = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-imagemin');
const pngquant = require('C:/Users/user/AppData/Roaming/npm/node_modules/imagemin-pngquant');
const imageminWebp = require('C:/Users/user/AppData/Roaming/npm/node_modules/imagemin-webp');
const changed = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-changed');

let beml = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-beml');

var layouts = require('C:/Users/user/AppData/Roaming/npm/node_modules/handlebars-layouts');
let compileHandlebars = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-compile-handlebars');
var fs = require('fs');



const image = require('gulp-image');


compileHandlebars.Handlebars.registerHelper(layouts(compileHandlebars.Handlebars));
compileHandlebars.Handlebars.registerHelper('compare', function(lvalue, rvalue, options) {

    if (arguments.length < 3)
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

    var operator = options.hash.operator || "==";

    var operators = {
        '==':       function(l,r) { return l == r; },
        '===':      function(l,r) { return l === r; },
        '!=':       function(l,r) { return l != r; },
        '<':        function(l,r) { return l < r; },
        '>':        function(l,r) { return l > r; },
        '<=':       function(l,r) { return l <= r; },
        '>=':       function(l,r) { return l >= r; },
        'typeof':   function(l,r) { return typeof l == r; }
    }

    if (!operators[operator])
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator "+operator);

    var result = operators[operator](lvalue,rvalue);

    if( result ) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }

});


const paths = {
    dest: ['./dest/'],
    scss: ['./app/scss/*.scss'],
    js: ['./app/js/*.js'],
    templates: [''],
    icons: ['./app/images/icons/*.svg'],
    images: ['./app/images/**/*.+(jpg|jpeg|png|svg|gif|ico|webp)']
}

var options = {};

options.autoprefixer = {
    browsers: [
        "Android 2.3",
        "Android >= 4",
        "Chrome >= 20",
        "Firefox >= 24",
        "Explorer >= 7",
        "iOS >= 6",
        "Opera >= 12",
        "Safari >= 6"
    ]
};

function getData () {
    return JSON.parse(fs.readFileSync('./app/templates/data.otk.json'));
};

// ++ templates
function compileHtml(cb) {
    var templateData = {
            info:  getData()
        },
        options = {
            partialsDirectory: [config.templatePartialPath],
            batch: [config.templatePartialPath]
        };
    return src(config.templatePath + "*.page.hbs")
        .pipe(changed(config.templateOutputPath))
        .pipe(compileHandlebars(templateData, options))
        .pipe(beml(config.beml))
        .pipe(regexRename(/\.page\.hbs$/, ".html"))
        .pipe(replace(/\uFEFF/ig, "")) //cut out zero width nbsp characters the compiler adds in
        .pipe(dest(config.templateOutputPath))
        .pipe(browserSync.reload({
            stream: true
        }))
    cb();
}
// -- templates


// ++ preprocess
function preprocess(cb) {
    return src('./app/scss/*.scss')
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer(options.autoprefixer))
        .pipe(sourcemaps.write('./dest/css/'))
        .pipe(dest('./dest/css/'))
        .pipe(browserSync.reload({
            stream: true
        }));
    cb();
}
// -- preprocess



// ++ iconizer
function iconizer(cb) {
    return src(paths.icons) // svg files for sprite
        .pipe(svgSprite({
            mode: {
                view: { // Activate the «view» mode
                    bust: false,
                    render: {
                        scss: true // Activate Sass output (with default options)
                    }
                },
                symbol: {
                    sprite: "./sprite.svg" //sprite file name
                }
            },
        }))
        .pipe(dest('./dest/images/'))
        .pipe(browserSync.reload({
            stream: true
        }));
    cb();
};
// -- iconizer



// ++ images
// function images(cb) {
//     return src(paths.images)
//         .pipe(changed('./dest/images'))
//         .pipe(imagemin({
//             use: [
//                 pngquant(),
//                 imageminWebp({
//                     quality: 50
//                 })
//             ],
//             optimizationLevel: 3,
//             progressive: true,
//             interlaced: true,
//             svgoPlugins: [{
//                 removeViewBox: false
//             }]
//         }))
//         .pipe(dest('./dest/images'))
//         .pipe(browserSync.reload({
//             stream: true
//         }));
//     cb();
// };
// -- images









function images(cb) {
    return src(paths.images)
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            jpegRecompress: true,
            mozjpeg: true,
            guetzli: true,
            gifsicle: true,
            svgo: true,
            concurrent: 10,
            quiet: true // defaults to false
        }))
        .pipe(dest('./dest/images'))
        .pipe(browserSync.reload({
            stream: true
        }));
    cb();
};
















function reloadJs(cb) {
    return src(paths.js)
        .pipe(dest(paths.dest + 'js'))
        .pipe(browserSync.reload({
            stream: true
        }))
    cb();
};

function startServer() {
    browserSync.init({
        open: false,
        injectChanges: true,
        port: 8765,
        server: {
            baseDir: "dest",
            directory: true
        }

    });
}

exports.watch = function () {
    startServer();
    watch(paths.scss, preprocess);
    watch(paths.js, reloadJs);
    watch(config.templates, compileHtml);
    watch(config.templatePartialPath, compileHtml);
    watch(paths.icons, iconizer);
    watch(paths.images, images);
}

// gulp.task('build', () => {
//     gulp.src(paths.images)
//     .pipe(changed('./dest/images'))
//     .pipe(imagemin({
//         optimizationLevel: 3,
//         progressive: true,
//         interlaced: true,
//         svgoPlugins: [{
//             removeViewBox: false
//         }],
//         use: [pngquant(), imageminWebp()]
//     }))
//     .pipe(dest('./dest/images'))
// })