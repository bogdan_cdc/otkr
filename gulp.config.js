module.exports = function () {
    var root = './app/';
    var assetRoot = root + '';
    var handlebarsRoot = assetRoot + 'templates/';

    var config = {
        templatePath: handlebarsRoot,
        templatePartialPath: handlebarsRoot + 'partials',
        templateOutputPath: handlebarsRoot + '../../dest/',
        templates: [
            handlebarsRoot + '**/*.hbs'
        ],
        beml        : {
            elemPrefix: '--',
            modPrefix : '-',
            modDlmtr  : '-'
        }
    };

    return config;
};