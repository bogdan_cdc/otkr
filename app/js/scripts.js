// ++++++++ mainPage slider

function swipedetect(el, callback) {

    var touchsurface = el,
        swipedir = 'none',
        startX,
        startY,
        distX,
        distY,
        threshold = 50, //required min distance traveled to be considered swipe
        restraint = 50, // maximum distance allowed at the same time in perpendicular direction
        allowedTime = 300, // maximum time allowed to travel that distance
        elapsedTime,
        startTime,
        handleswipe = callback;

    touchsurface.addEventListener('touchstart', function (e) {
        var touchobj = e.changedTouches[0];
        swipedir = 'none';
        var dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime();
        // record time when finger first makes contact with surface
    }, false)

    touchsurface.addEventListener('touchend', function (e) {
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
        elapsedTime = parseInt(new Date().getTime()) - parseInt(startTime) // get time elapsed
        if (elapsedTime <= allowedTime) { // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) { // 2nd condition for horizontal swipe met
                swipedir = (distX < 0) ? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
                // e.preventDefault()
            } else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) { // 2nd condition for vertical swipe met
                swipedir = (distY < 0) ? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
            }
        }

        handleswipe(swipedir)
    }, false)

    touchsurface.addEventListener('mousedown', function (e) {
        swipedir = 'none';
        startX = e.pageX;
        startY = e.pageY;
        // e.target.parentNode.style.opacity = '.5'
    })

    touchsurface.addEventListener('mouseup', function (e) {

        distX = e.pageX - startX;
        distY = e.pageY - startY;

        if (Math.abs(distX) >= threshold && Math.abs(distY) <= 100) { // 2nd condition for horizontal swipe met
            swipedir = (distX < 0) ? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
        } else if (Math.abs(distY) >= threshold && Math.abs(distX) <= 100) { // 2nd condition for vertical swipe met
            swipedir = (distY < 0) ? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
        }
        handleswipe(swipedir)
    })
}

$('.uvSlider').each(function (index, el) {
    for (var i = 0; i < $(el).find('.uvSliderItem').length; i++) {
        $(el).find('.uvSlider__pagination').append('<li class="uvSlider__pagination__item navSonar"><button class="sr-only" aria-controls="carousel">Toggle slide ' + (i + 1) + '</button></li>');
    }
    $(el).find('.uvSlider__pagination__item').eq($('.uvSliderItem.active').index()).addClass('active');
});

$('.uvSliderItem.active').each(function (index, el) {
    $(el).velocity({
        opacity: 1,
        complete: function () {
            $(el).attr('aria-hidden', 'false')
        }
    })
});

function uvSlideHide(act, dir) {
    var d = 1;
    if (dir == 'prev') {
        d = -1;
    }

    act.find('.uvSliderItem__mainText').velocity({
        translateX: -294 * d,
    }, {
        duration: 800
    });
    act.find('.uvSliderItem__mainImage').velocity({
        translateX: -294 * d,
    }, {
        duration: 700
    });
    act.velocity({
        opacity: [0, 1],
        complete: function () {
            act.attr('aria-hidden', 'true')
        }
    }, {
        duration: 800
    });
}

function uvSlideShowNext(act, nextAct, slideImage, slideText, dir, effect) {

    var d = 1;
    if (dir == 'prev') {
        d = -1;
    }

    nextAct.addClass('active');

    slideText.velocity({
        opacity: 0
    }, {
        duration: 0
    });

    nextAct.velocity({
        // translateX: [0, 294 * d],
        opacity: [1, 0],
        complete: function () {
            nextAct.attr('aria-hidden', 'false')

            if ($('._height_recalc').length) {
                nextAct.parents('.uvSliderItemsList').outerHeight(nextAct.find('._height_recalc').outerHeight())
            }
        }
    }, {
        duration: 800
    });

    nextAct.find('.ghost_text').velocity({
        translateX: [0, 294 * d],
        opacity: [1, 0],
    }, {
        duration: 1200,
        easing: [0.24, 0.57, 0.47, 0.8]
    });

    if (effect == undefined) {
        nextAct.find('.uvSliderItem__background').velocity({
            scale: [1, 1.1]
        }, {
            duration: 2000
        });
    } else if (effect == 'slide') {
        nextAct.find('.uvSliderItem__background').velocity({
            translateX: [0, -100 * d]
        }, {
            duration: 800
        });
    } else if (effect == 'fade') {
        nextAct.find('.uvSliderItem__background').velocity({
            opacity: [1, 0]
        }, {
            duration: 800
        });
    }

    slideImage.velocity({
        translateX: [0, -200 * d],
        opacity: [1, 0]
    }, {
        duration: 800
    });

    slideText.velocity({
        translateX: [0, -124 * d],
        opacity: [1, 0]
    }, {
        duration: 800
    });
}

function initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect) {

    if (uvItemsLength > 1) {


        activeItem.removeClass('active');

        $('.velocity-animating').each(function () {
            $.Velocity.mock = true;
            $(this).velocity("finish");
            $.Velocity.mock = false;
        });

        uvSlideHide(activeItem, dir);

        var d = 1;

        if (dir == 'prev') {
            d = -1;
        } else if (dir == 'stay') {
            d = 0;
        }

        var nextSlideIndex = activeItemIndex + d;
        if (toIndex != undefined) {
            nextSlideIndex = toIndex;
        } else if (nextSlideIndex < 0) {
            nextSlideIndex = uvItemsLength - 1;
        } else if (nextSlideIndex > uvItemsLength - 1) {
            nextSlideIndex = 0;
        }

        parent.find('.uvSlider__pagination__item').eq(nextSlideIndex).addClass('active').siblings().removeClass('active');

        var slide = parent.find('.uvSliderItem').eq(nextSlideIndex),
            slideImage = slide.find('.uvSliderItem__mainImage'),
            slideText = slide.find('.uvSliderItem__mainText');

        if (swipedir != undefined && swipedir == 'left' || swipedir == 'right') {
            uvSlideShowNext(activeItem, slide, slideImage, slideText, dir, effect);
        } else {
            uvSlideShowNext(activeItem, slide, slideImage, slideText, dir, effect);
        }

    }
}

$('.uvSlider__nav').on('click', function () {
    var dir = $(this).data('direction'),
        parent = $(this).parents('.uvSlider'),
        effect = parent.data('effect'),
        swipedir = undefined,
        toIndex = undefined,
        activeItem = parent.find('.uvSliderItem.active'),
        activeItemIndex = activeItem.index(),
        uvItemsLength = parent.find('.uvSliderItem').length;

    initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect);
});

$('.uvSlider__pagination__item').on('click', function () {
    var dir = 'next',
        parent = $(this).parents('.uvSlider'),
        effect = parent.data('effect'),
        swipedir = undefined,
        toIndex = $(this).index(),
        activeItem = parent.find('.uvSliderItem.active'),
        activeItemIndex = activeItem.index(),
        uvItemsLength = parent.find('.uvSliderItem').length;

    if ($(this).index() < parent.find('.uvSlider__pagination__item.active').index()) {
        dir = 'prev';
    }
    if (!$(this).hasClass('active')) {
        initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect);
    }
});

var swiperElements = document.getElementsByClassName('uvSliderItemsList');
// var swipeEl = document.getElementById('uvSliderItemsList');

for (let index = 0; index < swiperElements.length; index++) {
    const element = swiperElements[index];

    swipedetect(element, function (swipedir) {
        var dir = 'prev',
            parent = $(element).parents('.uvSlider'),
            effect = parent.data('effect'),
            swipedir = swipedir,
            toIndex = undefined,
            activeItem = parent.find('.uvSliderItem.active'),
            activeItemIndex = activeItem.index(),
            uvItemsLength = parent.find('.uvSliderItem').length;

        if (swipedir == 'left') {
            dir = 'next'
        }
        if (swipedir != 'none') {
            initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect);
        }
    });

}

// -------- mainPage slider

$(document).ready(function () {
    $('.main_nav--item-level_1').on('mouseenter', function (e) {
        var activeItemLeft = $(e.target).parent().position().left;
        $(e.target).parent().addClass('is-active');
        $(e.target).parent().find('.main_nav--list-level_2').css('left', activeItemLeft);
    });
    $('.main_nav--item-level_1').on('mouseleave', function (e) {
        $('.main_nav .is-active').removeClass('is-active')
    });
});

// ++++++++ forms

(function ($) {
    jQuery(document).ready(function ($) {

        var element = $('form').find('input, textarea, select');
        element.each(function () {
            if ($(this).val()) {
                $(this).siblings('label').addClass('active');
            }
        });

        $(document).on('focus', 'input, textarea, select', function (e) {
            setTimeout(function () {
                var $this = $(e.target);
                var noValue = !$this.val();

                if (noValue) {
                    $this.siblings('label').addClass('active');
                }
            }, 10);
        });
        $(document).on('input', 'input, textarea, select', function (e) {
            setTimeout(function () {
                var $this = $(e.target);

                $this.siblings('label').addClass('active');
            }, 10);
        });
        $(document).on('blur', 'input, textarea, select', function (e) {
            setTimeout(function () {

                var $this = $(e.target);
                var p_form = $this.closest('form');
                var element = p_form.find('input, textarea, select');
                element.each(function () {
                    if (!$(this).val()) {
                        $(this).siblings('label.awesomeLabel').removeClass('active');
                    }
                });
                var noValue = !$this.val();

                if (noValue) {
                    $this.siblings('label.awesomeLabel').removeClass('active');
                }
            }, 10);
        });
        $(document).on('animationstart', 'input, textarea, select', function (e) {
            setTimeout(function () {

                var $this = $(e.target);
                var noValue = !$this.val();

                if (noValue) {
                    $this.siblings('label').addClass('active');
                }
            }, 10);
        });
    });

    $("form").parsley({
        classHandler: function (Field) {
            var $classRec = Field.$element.parents('.inputContainer');
            return $classRec;
        },
        errorsContainer: function (Field) {
            var $container = Field.$element.parents('.inputContainer').find(".helperText");
            return $container;
        },
        errorsWrapper: '<div class="prsError"></div>',
        errorTemplate: '<span></span>'
    });
})(jQuery);

// -------- forms


// ++++++++ mobile menu

var pos_s = document.getElementsByClassName('pos_s')[0];

var disableScroll = false;
var scrollPos = 0;

function stopScroll() {
    disableScroll = true;
    scrollPos = $(window).scrollTop();
}

function enableScroll() {
    disableScroll = false;
}

jQuery(function ($) {

    $('.popupNavTrigger').on('click', function () {
        $(this).toggleClass('is-active');
		if (ps1) {
			ps1.destroy();
		} else if (ps2) {
			ps2.destroy();
        }
        ps1 = new PerfectScrollbar('._scrollable', {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true,
            scrollYMarginOffset: 1
        });
    });

    $(window).bind('scroll', function () {
        if (disableScroll) $(window).scrollTop(scrollPos);
    });
    $(window).bind('touchmove', function () {
        $(window).trigger('scroll');
    });
    var ps1,
        ps2;
    $('._level_1 span').on('click', function (event) {
        event.stopPropagation();
        ps1 = new PerfectScrollbar('._scrollable', {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });
    $('.hasChild').on('click', function (event) {
        // $('.mPopupNav__menu').addClass('fixed');
        if (ps1) {
            ps1.destroy();
        }
        if (ps2) {
            ps2.destroy();
        }
        $(this).find('.childItem').first().addClass('active');
    });
    $('.childItem__goBackBtn span').on('click', function (event) {
        event.stopPropagation();
        $(this).parents('.childItem').first().removeClass('active');
    });
    $('.mPopupNavScroll').each(function (index, el) {
        const ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });
    $('._close').on('click', function () {});

    $('.mPopupNav').on('hide.bs.modal', function () {
        if (ps1) {
            ps1.destroy();
        }
        if (ps2) {
            ps2.destroy();
        }
        $('.popupNavTrigger').removeClass('is-active');
        $('._popupNav').find('.childItem').each(function () {
            $(this).removeClass('active');
        });
    })

    function scrollItAll(whatToScroll, whatToDo) {
        var ps1_scrollable_content;
        ps1_scrollable_content = new PerfectScrollbar(whatToScroll, {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    }

    if ($('._scrollable_content').length) {
        $('._scrollable_content').each(function (index, el) {
            var k = '._scrollable_content' + index;
            scrollItAll(k)
        });
    }


    var mouseposy = 0,
        mouseposx = 0,
        firstTime = true;
    $('.main_categories > li').mouseover(function (e) {
        if (Math.abs(mouseposx - e.screenX) < Math.abs(mouseposy - e.screenY) || mouseposx - e.screenX > 0 && !firstTime) {
            $('.main_categories > li.active').removeClass('active');
            $(this).addClass('active');
        } else if (firstTime) {
            $('.main_categories > li.active').removeClass('active');
            $(this).addClass('active');
        }
        firstTime = false;
        mouseposx = e.screenX;
        mouseposy = e.screenY;
    });

    //set defaults for modals
    var scrollWidth = window.innerWidth - document.documentElement.clientWidth;
    var modalIsOpen = false,
        modalIsAnimated = false;

    function simpleModalShow(target) {
        if ($(target).length && !modalIsAnimated) { //no act while animating is in proggress or no target specified
            modalIsOpen = true;
            $('body').addClass('modal-open');
            $('body').css('padding-right', scrollWidth);
            $(target).velocity("fadeIn", {
                duration: 300,
                begin: function () {
                    modalIsAnimated = true;
                    $(target).trigger('modal_show');
                },
                complete: function () {
                    modalIsAnimated = false;
                    $(target).trigger('modal_shown');
                }
            });
            $(target).removeClass('fade');
            $(target).addClass('show');
        } else {
            console.error('the target modal does not exist');
        }
    }

    function simpleModalHide() {

        if (!modalIsAnimated) { //no act while animating is in proggress
            $('.modal.show').velocity("fadeOut", {
                duration: 300,
                begin: function () {
                    modalIsAnimated = true;
                    $('.modal.show').trigger('modal_hide');
                },
                complete: function () {
                    $('body').removeClass('modal-open');
                    $('body').css('padding-right', '');
                    $('.modal.show').removeClass('show');
                    modalIsOpen = false;
                    modalIsAnimated = false;
                    $('.modal.show').trigger('modal_hidden');
                }
            });
        }
    }

    $(document).on('click', '.toggleModal', function (event) {
        event.preventDefault();
        var target = $(this).data('target');

        if (modalIsOpen) {
            simpleModalHide();
        } else {
            simpleModalShow(target);
            $('body').addClass('scrolledUp');
        }
    });

    $('.modal:not(._popupNav), ._closeBtn').on('click', function (event) {
        if ($(event.target).attr('class') == $(this).attr('class')) { //work around bubbling
            event.preventDefault();
            simpleModalHide();
        }
    });

});

// -------- mobile menu



// ++++++++ sample steps


$(document).ready(function () {

    $('._steps_slider--indicator').css('width', 0);

    $('._questions--item').each(function () {
        if ($(this).index() == 1) {
            $(this).addClass('is_active');
        }
    });
    var steps_slider = $('._steps_slider--indicator');
    var steps_slides_length = $('._questions--item').length;

    $('.btn_next').on('click', function () {
        var currentItem = $('._questions--item.is_active');

        if (currentItem.index() == steps_slides_length - 1) {
            $(this).addClass('hidden');
            $('.btn_prev')
                .parent('.col-sm-6')
                .addClass('col-12')
                .removeClass('col-sm-6 text-sm-left');
        }

        $('.btn_prev').removeClass('hidden');


        steps_slider.css('width', (100 / steps_slides_length) * currentItem.index() + '%');

        currentItem.next('._questions--item').addClass('is_active');
        currentItem.removeClass('is_active');
    });
    $('.btn_prev').on('click', function () {
        var currentItem = $('._questions--item.is_active');
        if (currentItem.index() == 2) {
            $(this).addClass('hidden');
        }
        $('.btn_next').removeClass('hidden');
        $(this)
            .parent('.col-12')
            .removeClass('col-12')
            .addClass('col-sm-6 text-sm-left');
        steps_slider.css('width', (100 / steps_slides_length) * (currentItem.index() - 2) + '%');
        currentItem.prev('._questions--item').addClass('is_active');
        currentItem.removeClass('is_active');
    });
    $('._fake_submit').click(function (e) {
        e.preventDefault();

        steps_slider.css('width', '100%');

        $('._steps_change').hide();
        $('._questions--item.is_active').removeClass('is_active');
        $('._last--item').addClass('is_active');

        $('html,body').animate({
            scrollTop: 300
        }, 'slow');
    });
});


// -------- sample steps

// ++++++++ search

$('.search_opener').on('click', function(){
    $(this).parents('.mobile_search-desctop').addClass('mobile_search-opened');
});

$('.mobile_search--input').on('blur', function(){
    setTimeout(() => {
        $('.mobile_search-desctop').removeClass('mobile_search-opened');
    }, 100);
});

// -------- search